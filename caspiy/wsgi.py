"""
WSGI config for caspiy project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application

paths = ['/home/caspiy/caspiy', '/home/caspiy/caspiy/caspiy/apps/']

for path in paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'caspiy.settings')

application = get_wsgi_application()
